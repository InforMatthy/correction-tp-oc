import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  title = 'Mon petit blog en Angular';
  @Input() postList: [any];

  constructor() { }

  ngOnInit() {
  }

}
