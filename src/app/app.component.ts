import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = "Un blog en Angular !";
    presentation = "Bienvenu sur mon blog en Angular 7, et bonne visite !";

    posts = [
        {
            title: 'Un article',
            content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A animi, at atque dolore dolorum excepturi id iste itaque maxime molestiae non nostrum, odit perferendis quisquam ut. Dicta excepturi ipsum porro?  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores error explicabo harum impedit iure laudantium nobis obcaecati omnis pariatur possimus qui, quia quisquam quos reiciendis saepe sint, totam unde velit? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad aliquam delectus dignissimos dolores magni natus qui rem! Asperiores consequatur incidunt iure nulla numquam odit quibusdam quis rem sit tempore.',
            like: 0,
            dislike: 0,
            createdAt: new Date(2018, 11, 5)
        },
        {
            title: 'Un second article',
            content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus asperiores consectetur consequatur dolor doloribus dolorum, eum fuga itaque, nam natus odit optio porro quas recusandae rem rerum sapiente velit veritatis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque doloremque iusto minus quae rerum. Ab dolor eos illo iure, maiores molestias, nam non officia provident quo tempora, vel. Accusantium, quo?',
            like: 51,
            dislike: 24,
            createdAt: new Date(2019, 1, 18)
        },
        {
            title: 'Un troisième article',
            content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda cupiditate impedit ipsa natus placeat, sapiente. Accusamus alias cum dicta incidunt placeat quaerat quos repellendus, rerum suscipit tempora tenetur, voluptas? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias assumenda blanditiis deleniti, dignissimos doloremque doloribus enim eveniet illum magnam nihil, non officia provident rem saepe sapiente sed soluta veniam voluptatibus!',
            like: 72,
            dislike: 123,
            createdAt: new Date(2019, 3, 1)
        }
    ];
}
